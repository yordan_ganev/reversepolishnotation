/*
 * Author: Yordan
 * Date: 26.09.2020
 * About: Queue and stack class use
 *        arimetic expression to reverse polish notation
 */

// --- Libraries ---
#include <iostream>
#include <string>
#include <string.h>
#include <math.h>

#include "cstack.h"
#include "cqueue.h"

using namespace std;

// --- Definitions ---
#define println(var) cout << #var << " : " << var << endl;
#define MATH_VARIABLE 0

typedef struct {
    char symbol;
    float value;
} S_VAR_PAIR;

// --- Function prototypes ---
uint8_t getPriority(char c); // [numbers] < [+,-] < [/,*] < [(,)]

int main()
{
    /*
        "(a+b)*c-d*(e+f)", // ab+c*def+*-
        1 to 6
        =-35

        "(a+b)*c-d*e^(f+g)" // ab+c*defg+^*-
        7 to 1
        =-43
        "(a+b)*b-a*(b+b)" //
        2 3
        = 3
     */

    // get expression
    cout << "Enter expression.." << endl;
    string input;
    getline(cin, input);

    // display before
    println(input);

    // translate to reverse polish notation
    CStack<char> signs;
    CQueue<char> postfixExpression;

    for (uint32_t i = 0; i < input.length(); i++)
    {

        uint8_t tmpPriority = getPriority(input[i]);

        if (tmpPriority == MATH_VARIABLE)  // numbers go on queue
        {
            postfixExpression.push(input[i]);
        }
        else if (tmpPriority == (uint8_t)(')'))  // Removing bracket
        {
            // sign stack top pushed to expression queue
            // till reaching opening bracket
            while (getPriority(signs.top()) != (uint8_t)('('))
            {
                postfixExpression.push(signs.top());
                signs.pop();
            }

            signs.pop(); // remove opening bracket
        }
        else
        {

            // manage sign
            if ( signs.isEmpty() || tmpPriority >= getPriority(signs.top()) )
            {
                // Current sign is higher priority than
                // top stack sign => push it on top of stack
                signs.push(input[i]);
            }
            else
            {
                while ( !signs.isEmpty() && tmpPriority < getPriority(signs.top()) &&
                        ( getPriority(signs.top()) != (uint8_t)('(') ))
                {
                    // Stack top sign has higher priority
                    // push it on expression queue
                    postfixExpression.push(signs.top());
                    signs.pop();
                }

                // push current sign on the stack top
                signs.push(input[i]);
            }
        }

    }

    // passed threw input expression
    // finish reverse notation
    // push top ot signs stack to the expression queue
    while ( signs.isEmpty() != true )
    {
        postfixExpression.push(signs.top());
        signs.pop();
    }


    // display after
    string reversedExpression;
    while ( postfixExpression.isEmpty() != true)
    {
        reversedExpression += postfixExpression.front();
        postfixExpression.pop();
    }

    println(reversedExpression);

    // get variables value
    S_VAR_PAIR *varPair = new S_VAR_PAIR[reversedExpression.length()];
    uint16_t symbols = 0;

    for (uint32_t i = 0; i < reversedExpression.length(); i++)
    {
        if( getPriority(reversedExpression[i]) == MATH_VARIABLE )
        {
            bool newSymbol = true;

            for (uint16_t isSet = 0; isSet < symbols; isSet++)
            {
                if ( varPair[isSet].symbol == (char)reversedExpression[i] )
                {
                    newSymbol = false;
                    break;
                }
            }

            if ( newSymbol )
            {
                cout << "Enter value for '" << (char)reversedExpression[i] << "'.." << endl;
                cin >> varPair[symbols].value;
                varPair[symbols++].symbol = (char)reversedExpression[i];
            }
        }
    }


    CStack<float> calculate;
    for ( uint32_t i = 0; i < reversedExpression.length(); i++)
    {
        if( getPriority(reversedExpression[i]) == MATH_VARIABLE )
        {

            for (uint16_t isSet = 0; isSet < symbols; isSet++)
            {
                if ( varPair[isSet].symbol == (char)reversedExpression[i] )
                {
                    calculate.push(varPair[isSet].value);
                    break;
                }
            }

        }
        else
        {
            float top = calculate.top();
            calculate.pop();
            float bot = calculate.top();
            calculate.pop();

            // Push [BOT Operator TOP]
            switch (reversedExpression[i])
            {
            case '+':
                calculate.push(bot + top);
                break;
            case '-':
                calculate.push(bot - top);
                break;
            case '*':
                calculate.push(bot * top);
                break;
            case '/':
                calculate.push(bot / top);
                break;
            case '^':
                calculate.push(powf(bot, top));
                break;
            }

        }
    }

    println(calculate.top());

    return 0;
}

uint8_t getPriority( char c )
{
    switch (c)
    {
    // math priority
    case '+':
    case '-':
        return 1;
    case '*':
    case '/':
        return 2;
    case '^':
        return 3;
    case '(':
        return (uint8_t)('(');
    case ')':
        return (uint8_t)(')');

    // undefined chars
    // any other symbol treated as variable('a','b'..'z'+'0'..'9'+..)
    // TODO: filter
    default:
        return MATH_VARIABLE;
    }
}
