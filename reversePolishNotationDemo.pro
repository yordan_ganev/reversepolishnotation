TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
    postfixNotation.cpp
#    postfixNotationCalc.cpp

HEADERS += \
    cqueue.h \
    cstack.h
