/*
 *  Author: Yordan
 *  Date:   26.09.2020
 *  About:  Simple queue library
 */

#ifndef CQUEUE_H
#define CQUEUE_H

#include <inttypes.h>

// Queue list item
template<typename T>
struct S_QUEUE_ITEM
{
    T value;
    S_QUEUE_ITEM<T> *next = nullptr;
};

// --- Queue class ---
template<typename T>
class CQueue
{
private:
    S_QUEUE_ITEM<T>* first;
    S_QUEUE_ITEM<T>* last;

    uint32_t items;

public:
    // --- Class function declarations ---
    CQueue();
    ~CQueue();

    void push(const T& value);  // add new element at back
    void pop(); // delete the first element

    // --- Getters ---
    bool isEmpty();
    uint32_t size();  // elements count

    T front(); // returns first element's value
    T back();  // returns last element's value

};


// --- Class functions implementation ---
// Basic constructior
template<typename T>
CQueue<T>::CQueue()
    :first(nullptr), last(nullptr), items(0)
{}

// Destructor
template<typename T>
CQueue<T>::~CQueue()
{
    while(first != nullptr)
    {
        pop();
    }
}
template<typename T>
void CQueue<T>::push(const T& value)
{
    // create new element and set its value
    S_QUEUE_ITEM<T> *newItem = new S_QUEUE_ITEM<T>;
    newItem->value = value;


    // link the new item with the last in the queue
    // and then refer to the new element as last
    if (last != nullptr)
    {
        last->next = newItem;
    }
    else
    {
        first = newItem;
    }
    last = newItem;

    items++; // count elements
}


template<typename T>
void CQueue<T>::pop()
{
    if(first != nullptr)
    {
        // first element exist, get its adress
        S_QUEUE_ITEM<T> *ptrDel = first;

        // link to next before freeing memory of first
        first = first->next;

        // free memory of first by adress
        delete ptrDel;

        items--; // count elements

        if ( isEmpty() ) {
            last = nullptr; // removed last element
        }
    }
}

// --- Getters ---
template<typename T>
bool CQueue<T>::isEmpty()
{
    return !items;
}

template<typename T>
uint32_t CQueue<T>::size()
{
    return items;
}

template <typename T>
T CQueue<T>::front()
{
    return  first->value;
}

template <typename T>
T CQueue<T>::back()
{
    return  last->value;
}

#endif /* CQUEUE_H */
