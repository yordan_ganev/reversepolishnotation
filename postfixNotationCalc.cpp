﻿/*
 *  Author: Yordan
 *  Date:   26.09.2020
 *  About:  Queue and stack class use
 *          translate regular expression to postfix
 *          using stack evaluate expression
 *          ( works for binary operators + - * / ^ )
 */

// --- Libraries ---
#include <iostream>
#include <string>
#include <string.h>
#include <math.h>

#include "cstack.h"
#include "cqueue.h"

using namespace std;

// --- Definitions ---
#define println(var) cout << #var << " : " << var << endl;
#define MATH_NUMBER 0
#define END_STR     "end"

// --- Type definitions ---
typedef enum {
    T_NUMBER,
    T_SIGN
} E_TYPE;

typedef struct {
    E_TYPE type;
    float value;
    char sign;
} st_expression_node;

// --- Function prototypes ---
uint8_t getPriority(char c); // [numbers] < [+,-] < [/,*] < [(,)]
bool fromNumber(char c); // 0..9 + '.'

// @input: valid expression string
// @postfixExpression: EMPTY CQueue<st_expression_node>
// get valid postfix expression &postfixExpression
void toPostfixNotation ( CQueue<st_expression_node> &postfixExpression , const string& input);

// @postfixExpression: VALID postfix expression
float calculatePostfixExpression( CQueue<st_expression_node> &postfixExpression );


int main()
{
    /*
        // demo expressions
        "(12.5-4*5)", // -7.5
        "(12.5+4*5)-13445.4*2", // −26858,3
        "(5.0912-5*2^5)+1", // −153,9088
        "(4.1+6*1)*3^2-2^3" //82,9
        "(4.1+6*1)*3^2-2^(7-2*2)" //82,9
        "(1.5+3.6)*4.723-10.61*3^(5.53-2.224)" // −376,851653061
    */

    string input;

    do
    {   
        cout << endl << "Enter expression or \"" << END_STR <<"\" to exit.." << endl;
        getline(cin, input);

        if ( input == END_STR)
        {
            break;
        }

        // display before
        println(input);

        // translate to reverse polish notation
        CQueue<st_expression_node> postfixExpression;
        toPostfixNotation(postfixExpression, input);

        // calculate postfix expression with stack
        float result;
        result = calculatePostfixExpression(postfixExpression);

        //display after
        println(result);
    } while (input != END_STR);

    return 0;
}

uint8_t getPriority( char c )
{
    switch (c)
    {
    // math priority
    case '+':
    case '-':
        return 1;
    case '*':
    case '/':
        return 2;
    case '^':
        return 3;
    case '(':
        return (uint8_t)('(');
    case ')':
        return (uint8_t)(')');

    // undefined chars
    // any other symbol treated as variable('a','b'..'z'+'0'..'9'+..)
    default:
        return MATH_NUMBER;
    }
}

bool fromNumber( char c )
{
    return ( (c >= '0' && c <= '9') || c == '.' );
}

void toPostfixNotation ( CQueue<st_expression_node> &postfixExpression,  const string& input )
{
    CStack<char> signs;
    for (uint32_t i = 0; i < input.length(); i++)
    {

        uint8_t tmpPriority = getPriority(input[i]);
        if (tmpPriority == MATH_NUMBER)  // numbers go on queue
        {
            st_expression_node node;
            int numLength = 0;

            while (fromNumber(input[i+(++numLength)]) && (i+numLength) < input.length());

            node.type = T_NUMBER;
            node.value = stof(input.substr(i, numLength));
            if ( numLength)
            {
                i += numLength - 1;
            }

            postfixExpression.push(node);
        }
        else if (tmpPriority == (uint8_t)(')'))  // Removing bracket
        {
            // sign stack top pushed to expression queue
            // till reaching opening bracket
            while (getPriority(signs.top()) != (uint8_t)('('))
            {
                st_expression_node node;
                node.type = T_SIGN;
                node.sign = signs.top();
                signs.pop();

                postfixExpression.push(node);
            }

            signs.pop(); // remove opening bracket
        }
        else
        {
            // manage sign
            if ( signs.isEmpty() || tmpPriority >= getPriority(signs.top()))
            {
                // Current sign is higher priority than
                // top stack sign => push it on top of stack
                signs.push(input[i]);
            }
            else
            {
                // Put higher priority signs from stack on the expression queue
                // before
                while ( !signs.isEmpty() && tmpPriority < getPriority(signs.top()) &&
                       ( getPriority(signs.top()) != (uint8_t)('(') ))
                {

                    // Stack top sign has higher priority
                    // push it on expression queue

                    st_expression_node node;

                    node.type = T_SIGN;
                    node.sign = signs.top();
                    signs.pop();

                    postfixExpression.push(node);
                }

                // push current sign on the stack top
                signs.push(input[i]);
            }
        }

    }

    // passed threw input expression
    // finish reverse notation
    // push top ot signs stack to the expression queue
    while ( signs.isEmpty() != true )
    {
        st_expression_node node;

        node.type = T_SIGN;
        node.sign = signs.top();
        postfixExpression.push(node);
        signs.pop();
    }
}


float calculatePostfixExpression( CQueue<st_expression_node> &postfixExpression )
{
    CStack<float> resultStack;

    // Pass every node
    while ( postfixExpression.isEmpty() == false )
    {

        // Numbers go on top of stack
        if ( postfixExpression.front().type == T_NUMBER)
        {
            resultStack.push(postfixExpression.front().value);
            postfixExpression.pop();
        }
        else
        {
            float top = resultStack.top();
            resultStack.pop();

            float bot = resultStack.top();
            resultStack.pop();

            // Push [BOT Operator TOP]
            switch (postfixExpression.front().sign)
            {
            case '+':
                resultStack.push(bot + top);
                break;
            case '-':
                resultStack.push(bot - top);
                break;
            case '*':
                resultStack.push(bot * top);
                break;
            case '/':
                resultStack.push(bot / top);
                break;
            case '^':
                resultStack.push(powf(bot, top));
                break;
            }

            postfixExpression.pop(); // remove sign
        }
    }

    return resultStack.top();
}
