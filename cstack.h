/*
 *  Author: Yordan
 *  Date:   25.09.2020
 *  About:  Simple stack lib
 */
#ifndef CSTACK_H
#define CSTACK_H
#include <inttypes.h>

// Stack list item
template <typename T>
struct S_STACK_ITEM
{
    T value;
    S_STACK_ITEM<T> *previous = nullptr;
};

// --- Stack class ---
template <typename T>
class CStack
{
private:
    S_STACK_ITEM<T> *stackTop;
    uint32_t items;

public:
    // --- Function prototipes ---
    CStack();
    ~CStack();

    void push( const T& value); // add new element at top
    void pop(); // remove the element on top

    bool isEmpty();
    uint32_t size (); // elements count

    const T& top(); // returns top element's value
};

// --- Class functions implementation ---

// basic constructor
template <typename T>
CStack<T>::CStack()
    :stackTop(nullptr), items(0)
{}

// destructor
template <typename T>
CStack<T>::~CStack()
{
    while (stackTop != nullptr)
    {
        pop();
    }
}

template <typename T>
void CStack<T>::push(const T& value)
{
    // create new item
    S_STACK_ITEM<T> *newItem = new S_STACK_ITEM<T>;

    // set its value and link it to top element
    newItem->previous = stackTop;
    newItem->value = value;

    // refer to the new item as top element
    stackTop = newItem;

    items++; // count elements
}

template <typename T>
void CStack<T>::pop()
{
    if ( stackTop != nullptr)
    {
        // get top element's adress
        S_STACK_ITEM<T> *popItem = stackTop;

        // change top element
        stackTop = stackTop->previous;

        // then free last top element memory from adress
        delete popItem;

        items--; // count items
    }
}


// --- Getters ---
template <typename T>
const T& CStack<T>::top()
{
    return stackTop->value;
}


template <typename T>
uint32_t CStack<T>::size()
{
  return items;
}

template <typename T>

bool CStack<T>::isEmpty()
{
   return !items;
};

#endif // CSTACK_H
